package ddp2.tp2;

class Informasi {
    private int jumlahHitam = 0;
    private int jumlahPutih = 0;
    private boolean menang = false;
    private String[]tebakanRemain = new String[4];
    private String[]kunciRemain = new String[4];
    private String feedback;

    public void info(String newFeedback){
        this.feedback = newFeedback;
        this.jumlahHitam = 0;
        this.jumlahPutih = 0;
        for (int i = 0; i < feedback.length(); i++){
            if (feedback.equals("Hitam")){
                this.jumlahHitam++;
            }
            else if (feedback.equals("Putih")){
            this.jumlahPutih++;
            }
        }

        if (jumlahHitam == 4){
            this.menang = true;
        }
        
    }

    public String cetakHasil(int jumlahTebakan, boolean giveUp, int score) {
        if (giveUp == true) {
            return "Wah, anda menyerah setelah " + jumlahTebakan + " tebakan.\nKode rahasianya adalah " + this.feedback;
        }
        else if (this.menang == false && giveUp == false) {
            return "Belum tepat. Hitam: " + this.getJumlahHitam() + " Putih: " + this.getJumlahPutih();
        }
        else if (this.menang == true) {
            return "Ya selamat, anda berhasil menjawab dalam " + jumlahTebakan + " tebakan. Score anda : " + score;
        }
        return "";
    }

    public Informasi() {
    }

    public int getJumlahHitam() {
        return this.jumlahHitam;
    }

    public int getJumlahPutih() {
        return this.jumlahPutih;
    }

    public boolean getMenang() {
        return this.menang;
    }

    public int[] cekInfo(String guess, String kunci){

        cekHitam(guess, kunci);
        cekPutih();
        int[] res = {jumlahHitam, jumlahPutih};

        return res;
    }
    
    public int cekHitam(String guess, String kunci){
        
        for (int i = 0; i < 4; i++){
            if (guess.charAt(i) == kunci.charAt(i)){
                jumlahHitam++;
            }
            else{
                char temp1 = guess.charAt(i);
                char temp2 = kunci.charAt(i);
                tebakanRemain[i] = Character.toString(temp1);
                kunciRemain[i] = Character.toString(temp2);
            }
        }

        if (jumlahHitam == 4){
            this.menang = true;
        }
        return jumlahHitam;
        
    }

    public int cekPutih(){
        for (int i = 0; i < 4; i++){
            if (tebakanRemain[i] != null){
                for (int j = 0; j < 4; j++){
                    if (kunciRemain[j] != null){
                        if (tebakanRemain[i].equals(kunciRemain[j])){
                            jumlahPutih++;
                        }
                    }
                }
            }
        }  
        return jumlahPutih;       
    }

}


    
