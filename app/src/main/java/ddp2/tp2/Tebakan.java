package ddp2.tp2;

class Tebakan{
    private String guess;
    Informasi info;


    public Tebakan(String guess){
        this.guess = guess;
        info = new Informasi();
    }

    public String getGuess(){
        return this.guess;
    }

    public Informasi getInfo(){
        return this.info;
    }
    
}
