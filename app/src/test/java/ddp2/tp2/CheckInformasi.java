package ddp2.tp2;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions.*;


class CheckInformasi {
    @Test void testHitam() {
        Tebakan tebakan = new Tebakan("MMMH");
        int[] res = {1,0};
        assertEquals(tebakan.getInfo().cekInfo("MMMH", "HHHH"), res);
    }
}

